It is web-based programming interface, which can be used for automatic validation and evaluation of C/C++ programs.

 System provides online editor, where the user can submit the code. The source code should maintain a specific structure for both input and output.
 System provides Upload option for uploading files, along with the editor.
 System compiles the code in the server, on submit.
 It displays the output of the code and displays the result “pass” or “fail” based on comparison with the expected output.
 It will also display any erroneous messages or warnings.
 The system will run the program for multiple input test cases and compare the outputs with respective expected results.
 It will display the particular test case failure, if any.
 All the submitted source codes are saved temporarily in server.
 This database of source code can be run for plagiarism check.
 User/Client need not have compilers installed.

Express Web Framework
Written in JavaScript 
Server-side web and mobile application framework for Node.js
Lightweight framework with extra, built-in web application features 
Templating engines -Jade and EJS, which facilitate the flow of data into a website structure
Operating system - It’s cross-platform, so it’s not limited to one OS
